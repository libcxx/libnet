CXX = g++
CXXFLAGS = -Wall -Wextra -std=c++23
LDFLAGS = -I .

SRCDIR = test
BUILDDIR = build
SOURCES = $(wildcard $(SRCDIR)/*.cxx)
OBJECTS = $(SOURCES:$(SRCDIR)/%.cxx=$(BUILDDIR)/%.o)
TARGETS = $(OBJECTS:$(BUILDDIR)/%.o=$(BUILDDIR)/%)

all: $(TARGETS)

$(BUILDDIR)/%: $(BUILDDIR)/%.o
	@mkdir -p $(BUILDDIR)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -o $@ $^

$(BUILDDIR)/%.o: $(SRCDIR)/%.cxx
	@mkdir -p $(BUILDDIR)
	$(CXX) $(CXXFLAGS) $(LDFLAGS) -c -o $@ $<

clean:
	rm -rf $(BUILDDIR)/*

.PHONY: all clean
