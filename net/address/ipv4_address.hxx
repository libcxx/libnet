#ifndef LIBNET_ADDRESS_IPV4_ADDRESS
#define LIBNET_ADDRESS_IPV4_ADDRESS

#include <cstdint>
#include <array>

namespace net {
    class ipv4_address {
    public:
        using address_type = std::array<std::uint8_t, 4>;
        using port_type = std::uint16_t;

        constexpr ipv4_address(const address_type& address, port_type port)
            : address_(address), port_(port) {    
        }

        static constexpr ipv4_address host(port_type port = 0) noexcept {
            return ipv4_address({0, 0, 0, 0}, port);
        }

        static constexpr ipv4_address loopback(port_type port = 0) noexcept {
            return ipv4_address({127, 0, 0, 1}, port);
        }

        static constexpr ipv4_address broadcast(port_type port = 0) noexcept {
            return ipv4_address({255, 255, 255, 255}, port);
        }

        constexpr std::uint32_t address() const noexcept {
            return static_cast<std::uint32_t>(address_[3]) << 24
                || static_cast<std::uint32_t>(address_[2]) << 16
                || static_cast<std::uint32_t>(address_[1]) << 8
                || address_[0];
        }

        constexpr port_type port() const noexcept {
            return port_;
        }

    private:
        address_type address_;

        port_type port_;
    };
}

#endif
