#ifndef LIBNET_ADAPTER_ADAPTER
#define LIBNET_ADAPTER_ADAPTER

#include "net/socket/socket_expected.hxx"

namespace net {
    template <typename Socket, typename Adapter>
    auto operator|(const socket_expected<Socket>& socket, const Adapter& adapter) noexcept {
        return adapter(socket);
    }
    
    template <typename Socket, typename Adapter>
    auto operator|(socket_expected<Socket>&& socket, const Adapter& adapter) noexcept {
        return adapter(std::move(socket));
    }
}

#endif
