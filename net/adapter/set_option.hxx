#ifndef LIBNET_ADAPTER_SET_OPTION
#define LIBNET_ADAPTER_SET_OPTION

#include "net/constants/option.hxx"
#include "net/socket/socket_expected.hxx"

namespace net {
    namespace detail_ {
        class set_option_closure {
        public:
            template <typename SocketExpected, typename... Args>
            auto operator()(SocketExpected&& sockexp, Args&&... args) const noexcept {
                return sockexp.and_then([&](auto&& socket) { return socket.set_option(std::forward<Args>(args)...); });
            }
        };
    }

    class set_option_adapter {
    public:
        template <write_option WriteOption, typename T>
        auto operator()(WriteOption option, T&& value) const noexcept {
            return [option, value = std::forward<T>(value)]<typename SocketExpected>(SocketExpected&& sockexp) {
                return detail_::set_option_closure()(sockexp, option, value); 
            };
        }
    };

    constexpr inline set_option_adapter set_option;
}

#endif
