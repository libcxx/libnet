#ifndef LIBNET_ADAPTER_CONNECT
#define LIBNET_ADAPTER_CONNECT

namespace net {
    namespace detail_ {
        template <typename Address>
        class connect_closure {
        public:
            constexpr connect_closure(const Address& address) noexcept
                : address_(address) {    
            }

            constexpr connect_closure(Address&& address) noexcept
                : address_(std::move(address)) {
            }

            template <typename Socket>
            auto operator()(socket_expected<Socket>&& socket) const noexcept {
                return socket.and_then([&](auto&& s) { return s.connect(address_); });
            }

        private:
            Address address_;
        };
    }

    class connect_adapter {
    public:
        template <typename Address>
        auto operator()(Address&& address) const noexcept {
            return detail_::connect_closure(std::forward<Address>(address));
        }
    };

    constexpr inline connect_adapter connect;
}

#endif
