#ifndef LIBNET_ADAPTER_BIND
#define LIBNET_ADAPTER_BIND

#include "net/socket/socket_expected.hxx"

namespace net {
    namespace detail_ {
        template <typename Address>
        class bind_closure {
        public:
            constexpr bind_closure(const Address& address) noexcept
                : address_(address) {    
            }

            constexpr bind_closure(Address&& address) noexcept
                : address_(std::move(address)) {
            }

            template <typename Socket>
            auto operator()(socket_expected<Socket>&& socket) const noexcept {
                return socket.and_then([&](auto&& s) { return s.bind(address_); });
            }

        private:
            Address address_;
        };
    }

    class bind_adapter {
    public:
        template <typename Address>
        auto operator()(Address&& address) const noexcept {
            return detail_::bind_closure(std::forward<Address>(address));
        }
    };

    constexpr inline bind_adapter bind;
}

#endif
