#ifndef LIBNET_ADAPTER_LISTEN
#define LIBNET_ADAPTER_LISTEN

#include "net/socket/socket_expected.hxx"

namespace net {
    namespace detail_ {
        class listen_closure {
        public:
            constexpr listen_closure(int backlog) noexcept
                : backlog_(backlog) {    
            }

            template <typename Socket>
            auto operator()(socket_expected<Socket>&& socket) const noexcept {
                return socket.and_then([&](auto&& s) { return s.listen(backlog_); });
            }

        private:
            int backlog_;
        };
    }

    class listen_adapter {
    public:
        auto operator()(int backlog) const noexcept {
            return detail_::listen_closure(backlog);
        }
    };

    constexpr inline listen_adapter listen;
}

#endif
