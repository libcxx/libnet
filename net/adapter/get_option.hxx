#ifndef LIBNET_ADAPTER_GET_OPTION
#define LIBNET_ADAPTER_GET_OPTION

#include <functional>
#include "net/constants/option.hxx"
#include "net/socket/socket_expected.hxx"

namespace net {
    namespace detail_ {
        class get_option_closure {
        public:
            template <typename SocketExpected, typename... Args>
            auto operator()(SocketExpected&& sockexp, Args&&... args) const noexcept {
                return sockexp.and_then([&](auto&& socket) { return socket.get_option(std::forward<Args>(args)...); });            
            }
        };
    }

    class get_option_adapter {
    public:
        template <read_option ReadOption>
        auto operator()(ReadOption option) const noexcept {
            return [option]<typename SocketExpected>(SocketExpected&& sockexp) {
                return detail_::get_option_closure()(std::forward<SocketExpected>(sockexp), option);
            };
        }

        template <read_option ReadOption>
        auto operator()(ReadOption option, typename ReadOption::underlying_type& value) const noexcept {
            return [option, &value]<typename SocketExpected>(SocketExpected&& sockexp) {
                return detail_::get_option_closure()(std::forward<SocketExpected>(sockexp), option, value);
            };
        }
    };

    constexpr inline get_option_adapter get_option;
}

#endif
