#ifndef LIBNET_ADAPTER_ACCEPT
#define LIBNET_ADAPTER_ACCEPT

#include "net/socket/socket_expected.hxx"

namespace net {
    namespace detail_ {
        class accept_closure {
        public:
            template <typename Socket>
            auto operator()(const socket_expected<Socket>& socket) const noexcept {
                return socket.and_then([](const auto& s) { return s.accept(); });
            }
        };
    }

    class accept_adapter {
    public:
        template <typename Socket>
        auto operator()(const socket_expected<Socket>& socket) const noexcept {
            return detail_::accept_closure()(socket);
        }
    };

    constexpr inline accept_adapter accept;
}

#endif
