#ifndef LIBNET_SOCKET_SOCKET_CONNECTED
#define LIBNET_SOCKET_SOCKET_CONNECTED

#include <span>
#include <sys/socket.h>
#include "net/socket/socket.hxx"
#include "net/socket/socket_base.hxx"
#include "net/socket/socket_state.hxx"

namespace net {
    template <typename Traits>
    class socket<connected_t, Traits> : public socket_base {
    public:
        using traits_type = Traits;

        constexpr socket(native_type socket) noexcept
            : socket_base(socket) {
        }

        template <typename State>
        constexpr socket(socket<State, traits_type>&& rhs) noexcept
            : socket_base(std::move(rhs)) {
        }

        ~socket() noexcept {
            socket_base::~socket_base();
        }

        template <typename R>
        void send(R&& range) noexcept {
            std::span buffer(range);
            ::send(native_handle(), buffer.data(), buffer.size_bytes(), 0);
        }

        template <typename R>
        void receive(R& range) noexcept {
            std::span buffer(range);
            ::recv(native_handle(), buffer.data(), buffer.size_bytes(), 0);
        }
    };
}

#endif
