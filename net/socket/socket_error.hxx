#ifndef LIBNET_SOCKET_SOCKET_ERROR
#define LIBNET_SOCKET_SOCKET_ERROR

#include <cerrno>
#include <system_error>

namespace net {
    /// @brief Returns the current socket error.
    std::error_code socket_error() noexcept {
        return std::make_error_code(static_cast<std::errc>(errno));
    }
}

#endif
