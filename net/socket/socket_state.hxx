#ifndef LIBNET_SOCKET_SOCKET_STATE
#define LIBNET_SOCKET_SOCKET_STATE

namespace net {
    /// @brief Tag for the unconnected socket state.
    struct unconnected_t {
    };

    /// @brief Tag for the bound socket state.
    struct bound_t {
    };

    /// @brief Tag for the listening socket state.
    struct listening_t {
    };

    /// @brief Tag for the connected socket state.
    struct connected_t {
    };
}

#endif
