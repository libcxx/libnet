#ifndef LIBNET_SOCKET_SOCKET
#define LIBNET_SOCKET_SOCKET

namespace net {
    /// @brief A networking socket.
    /// @tparam State The socket state.
    /// @tparam Traits The socket traits.
    template <typename State, typename Traits>
    class socket;
}

#endif
