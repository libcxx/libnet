#ifndef LIBNET_SOCKET_SOCKET_BASE
#define LIBNET_SOCKET_SOCKET_BASE

#include <utility>
#include <unistd.h>
#include "net/constants/option.hxx"
#include "net/socket/socket_expected.hxx"

namespace net {
    class socket_base {
    public:
        using native_type = int;

        static constexpr inline native_type invalid = -1;

        constexpr socket_base(native_type socket) noexcept : socket_(socket) {
        }

        constexpr socket_base(socket_base&& rhs) noexcept : socket_(rhs.release()) {
        }

        ~socket_base() noexcept {
            if (*this) {
                ::close(socket_);
            }
        }

        constexpr native_type native_handle() const noexcept {
            return socket_;
        }

        [[nodiscard]] constexpr native_type release() noexcept {
            return std::exchange(socket_, invalid);
        }

        template <typename S, read_option O>
        auto get_option(this const S& socket, O option) noexcept {
            using underlying_t = typename O::underlying_type;
            underlying_t optval;
            ::socklen_t optlen(sizeof(optval));
            auto value = ::getsockopt(socket.native_handle(), option.level(), option, &optval, &optlen);
            return make_socket_expected<underlying_t>(value, std::move(optval));
        }

        template <typename S, read_option O>
        auto get_option(this S&& socket, O option, typename O::underlying_type& optval) noexcept {
            using socket_t = std::decay_t<S>;
            ::socklen_t optlen(sizeof(optval));
            auto value = ::getsockopt(socket.native_handle(), option.level(), option, &optval, &optlen);
            return make_socket_expected<socket_t>(value, std::move(socket));
        }

        template <typename S, write_option O>
        auto set_option(this S&& socket, O option, const typename O::underlying_type& optval) noexcept {
            using socket_t = std::decay_t<S>;
            auto value = ::setsockopt(socket.native_handle(), option.level(), option, &optval, sizeof(optval));
            return make_socket_expected<socket_t>(value, std::move(socket));
        }

        constexpr operator bool() const noexcept {
            return native_handle() != invalid;
        }

        constexpr socket_base& operator=(socket_base&& rhs) noexcept {
            socket_ = rhs.release();
            return *this;
        }

        constexpr socket_base(const socket_base&) noexcept = delete;

        constexpr socket_base& operator=(const socket_base&) noexcept = delete;

    private:
        native_type socket_;
    };
}

#endif
