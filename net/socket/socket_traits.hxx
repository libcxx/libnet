#ifndef LIBNET_SOCKET_SOCKET_TRAITS
#define LIBNET_SOCKET_SOCKET_TRAITS

#include <netinet/ip.h>
#include "net/constants/family.hxx"

namespace net {
    template <typename Family, typename Protocol>
    class socket_traits_base {
    public:
        using family_type = Family;
        using protocol_type = Protocol;
        using address_type = typename family_type::address_type;
    };

    template <typename Family, typename Protocol>
    class socket_traits;

    template <typename Protocol>
    class socket_traits<detail_::ipv4_t, Protocol> : public socket_traits_base<detail_::ipv4_t, Protocol> {
    private:
        using base_type = socket_traits_base<detail_::ipv4_t, Protocol>;

    public:
        static auto make_address(const typename base_type::address_type& address) noexcept {
            ::sockaddr_in addr;
            addr.sin_family = ipv4;
            addr.sin_addr.s_addr = ::htonl(address.address());
            addr.sin_port = ::htons(address.port());
            return addr;
        }
    };
}

#endif
