#ifndef LIBNET_SOCKET_SOCKET_UNCONNECTED
#define LIBNET_SOCKET_SOCKET_UNCONNECTED

#include <sys/socket.h>
#include "net/socket/socket.hxx"
#include "net/socket/socket_base.hxx"
#include "net/socket/socket_bound.hxx"
#include "net/socket/socket_connected.hxx"

namespace net {
    template <typename Traits>
    class socket<unconnected_t, Traits> : public socket_base {
    public:
        using traits_type = Traits;

        constexpr socket(native_type socket) noexcept
            : socket_base(socket) {
        }

        template <typename State>
        constexpr socket(socket<State, traits_type>&& rhs) noexcept
            : socket_base(std::move(rhs)) {
        }

        ~socket() noexcept {
            socket_base::~socket_base();
        }

        auto bind(const typename traits_type::address_type& address) noexcept {
            using socket_t = socket<bound_t, traits_type>;
            auto addr = traits_type::make_address(address);
            auto value = ::bind(native_handle(), reinterpret_cast<::sockaddr*>(&addr), sizeof(addr));
            return make_socket_expected<socket_t>(value, std::move(*this));
        }

        auto connect(const typename traits_type::address_type address) noexcept {
            using socket_t = socket<connected_t, traits_type>;
            auto addr = traits_type::make_address(address);
            auto value = ::connect(native_handle(), reinterpret_cast<::sockaddr*>(&addr), sizeof(addr));
            return make_socket_expected<socket_t>(value, std::move(*this));
        }
    };
}

#endif
