#ifndef LIBNET_SOCKET_SOCKET_BOUND
#define LIBNET_SOCKET_SOCKET_BOUND

#include <sys/socket.h>
#include "net/socket/socket.hxx"
#include "net/socket/socket_base.hxx"
#include "net/socket/socket_expected.hxx"
#include "net/socket/socket_listening.hxx"

namespace net {
    template <typename Traits>
    class socket<bound_t, Traits> : public socket_base {
    public:
        using traits_type = Traits;

        constexpr socket(native_type socket) noexcept
            : socket_base(socket) {
        }

        template <typename State>
        constexpr socket(socket<State, traits_type>&& rhs) noexcept
            : socket_base(std::move(rhs)) {
        }

        ~socket() noexcept {
            socket_base::~socket_base();
        }

        auto listen(int backlog) noexcept {
            using socket_t = socket<listening_t, traits_type>;
            auto value = ::listen(native_handle(), backlog);
            return make_socket_expected<socket_t>(value, std::move(*this));
        }
    };
}

#endif
