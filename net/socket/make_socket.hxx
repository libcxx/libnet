#ifndef LIBNET_SOCKET_MAKE_SOCKET
#define LIBNET_SOCKET_MAKE_SOCKET

#include <sys/socket.h>
#include "net/constants/family.hxx"
#include "net/constants/protocol.hxx"
#include "net/socket/socket.hxx"
#include "net/socket/socket_expected.hxx"
#include "net/socket/socket_traits.hxx"
#include "net/socket/socket_unconnected.hxx"

namespace net {
    template <family Family, protocol Protocol>
    auto make_socket(Family family, Protocol protocol) noexcept {
        using socket_t = socket<unconnected_t, socket_traits<Family, Protocol>>;
        auto value = ::socket(family, protocol, 0);
        return make_socket_expected<socket_t>(value);
    }
}

#endif
