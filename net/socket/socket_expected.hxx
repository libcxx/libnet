#ifndef LIBNET_SOCKET_SOCKET_EXPECTED
#define LIBNET_SOCKET_SOCKET_EXPECTED

#include <expected>
#include <system_error>
#include "net/socket/socket_error.hxx"

namespace net {
    template <typename Socket>
    using socket_expected = std::expected<Socket, std::error_code>;

    template <typename T>
    auto make_socket_expected(int value) noexcept {
        using expected = socket_expected<T>;
        return value != -1 ? expected(value) : expected(std::unexpected(socket_error()));
    }

    template <typename T, typename U>
    auto make_socket_expected(int value, U&& exp) noexcept {
        using expected = socket_expected<T>;
        return value != -1 ? expected(std::forward<U>(exp)) : expected(std::unexpected(socket_error()));
    }
}

#endif
