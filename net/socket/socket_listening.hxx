#ifndef LIBNET_SOCKET_SOCKET_LISTENING
#define LIBNET_SOCKET_SOCKET_LISTENING

#include <sys/socket.h>
#include "net/socket/socket.hxx"
#include "net/socket/socket_base.hxx"
#include "net/socket/socket_connected.hxx"
#include "net/socket/socket_expected.hxx"

namespace net {
    template <typename Traits>
    class socket<listening_t, Traits> : public socket_base {
    public:
        using traits_type = Traits;

        constexpr socket(native_type socket) noexcept
            : socket_base(socket) {
        }

        template <typename State>
        constexpr socket(socket<State, traits_type>&& rhs) noexcept
            : socket_base(std::move(rhs)) {
        }

        ~socket() noexcept {
            socket_base::~socket_base();
        }

        auto accept() const noexcept {
            using socket_t = socket<connected_t, traits_type>;
            auto value = ::accept4(native_handle(), nullptr, nullptr, 0);
            return make_socket_expected<socket_t>(value);
        }
    };
}

#endif
