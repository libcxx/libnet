#ifndef LIBNET_CONSTANTS_OPTION
#define LIBNET_CONSTANTS_OPTION

#ifdef _WIN32
    #include <winsock2.h>
#else
    #include <sys/socket.h>
#endif
#include <concepts>
#include <type_traits>
#include "net/constants/level.hxx"

namespace net {
    namespace detail_ {
        /// @brief The native option type
        using native_option = int;

        /// @brief A base class for option types
        /// @tparam O The option value
        template <auto L, native_option O> requires level<decltype(L)>
        struct option_constant : std::integral_constant<native_option, O> {
            static constexpr inline auto level = L;
        };

        /// @brief Permit sending of broadcast datagrams
        struct broadcast_t final : option_constant<socket_level, SO_BROADCAST> {
            using underlying_type = int;
            static constexpr bool get = true;
            static constexpr bool set = true;
        };

        /// @brief Enable debug tracing
        struct debug_t final : option_constant<socket_level, SO_DEBUG> {
            using underlying_type = int;
            static constexpr bool get = true;
            static constexpr bool set = true;
        };

        /// @brief Bypass route table lookup
        struct dont_route_t final : option_constant<socket_level, SO_DONTROUTE> {
            using underlying_type = int;
            static constexpr bool get = true;
            static constexpr bool set = true;
        };

        /// @brief Get pending error and clear
        struct error_t final : option_constant<socket_level, SO_ERROR> {
            using underlying_type = int;
            static constexpr bool get = true;
            static constexpr bool set = false;
        };

        /// @brief Periodically test if connection still alive
        struct keep_alive_t final : option_constant<socket_level, SO_KEEPALIVE> {
            using underlying_type = int;
            static constexpr bool get = true;
            static constexpr bool set = true;
        };

        /// @brief Linger on close if data to send
        struct linger_t final : option_constant<socket_level, SO_LINGER> {
            using underlying_type = ::linger;
            static constexpr bool get = true;
            static constexpr bool set = true;
        };

        /// @brief Leave received out-of-band data inline
        struct oob_inline_t final : option_constant<socket_level, SO_OOBINLINE> {
            using underlying_type = int;
            static constexpr bool get = true;
            static constexpr bool set = true;
        };

        /// @brief Receive buffer size
        struct rcvbuf_t final : option_constant<socket_level, SO_RCVBUF> {
            using underlying_type = int;
            static constexpr bool get = true;
            static constexpr bool set = true;
        };

        /// @brief Send buffer size
        struct sndbuf_t final : option_constant<socket_level, SO_SNDBUF> {
            using underlying_type = int;
            static constexpr bool get = true;
            static constexpr bool set = true;
        };

        /// @brief Receive buffer low-water mark
        struct rcvlowat_t final : option_constant<socket_level, SO_RCVLOWAT> {
            using underlying_type = int;
            static constexpr bool get = true;
            static constexpr bool set = true;
        };

        /// @brief Send buffer low-water mark
        struct sndlowat_t final : option_constant<socket_level, SO_SNDLOWAT> {
            using underlying_type = int;
            static constexpr bool get = true;
            static constexpr bool set = true;
        };

        /// @brief Receive timeout
        struct rcvtimeo_t final : option_constant<socket_level, SO_RCVTIMEO> {
            using underlying_type = ::timeval;
            static constexpr bool get = true;
            static constexpr bool set = true;
        };

        /// @brief Send timeout
        struct sndtimeo_t final : option_constant<socket_level, SO_SNDTIMEO> {
            using underlying_type = int;
            static constexpr bool get = true;
            static constexpr bool set = true;
        };

        /// @brief Allow local address reuse
        struct reuse_addr_t final : option_constant<socket_level, SO_REUSEADDR> {
            using underlying_type = int;
            static constexpr bool get = true;
            static constexpr bool set = true;
        };

        /// @brief Allow local port reuse
        struct reuse_port_t final : option_constant<socket_level, SO_REUSEPORT> {
            using underlying_type = int;
            static constexpr bool get = true;
            static constexpr bool set = true;
        };

        /// @brief Get socket type
        struct socket_type_t final : option_constant<socket_level, SO_TYPE> {
            using underlying_type = int;
            static constexpr bool get = true;
            static constexpr bool set = false;
        };
    }

    template <typename T>
    concept option = std::derived_from<T, detail_::option_constant<T::level, T::value>>;

    template <typename T>
    concept read_option = option<T> && std::same_as<std::bool_constant<T::get>, std::true_type>;

    template <typename T>
    concept write_option = option<T> && std::same_as<std::bool_constant<T::set>, std::true_type>;

    constexpr inline detail_::broadcast_t broadcast;

    constexpr inline detail_::debug_t debug;

    constexpr inline detail_::dont_route_t dont_route;

    constexpr inline detail_::error_t error;

    constexpr inline detail_::keep_alive_t keep_alive;

    constexpr inline detail_::linger_t linger;

    constexpr inline detail_::oob_inline_t oob_inline;

    constexpr inline detail_::rcvbuf_t rcvbuf;

    constexpr inline detail_::sndbuf_t sndbuf;

    constexpr inline detail_::rcvlowat_t rcvlowat;

    constexpr inline detail_::sndlowat_t sndlowat;

    constexpr inline detail_::rcvtimeo_t rcvtimeo;

    constexpr inline detail_::sndtimeo_t sndtimeo;

    constexpr inline detail_::reuse_addr_t reuse_addr;

    constexpr inline detail_::reuse_port_t reuse_port;

    constexpr inline detail_::socket_type_t socket_type;
}

#endif
