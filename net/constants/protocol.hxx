#ifndef LIBNET_CONSTANTS_PROTOCOL
#define LIBNET_CONSTANTS_PROTOCOL

#ifdef _WIN32
    #include <winsock2.h>
#else
    #include <sys/socket.h>
#endif
#include <concepts>
#include <type_traits>

namespace net {
    namespace detail_ {
        /// @brief The native protocol type
        using native_protocol = int;

        /// @brief A base class for protocol types
        /// @tparam P The protocol value
        template <native_protocol P>
        struct protocol_constant : std::integral_constant<native_protocol, P> {
        };

        /// @brief TCP protocol type
        struct tcp_t final : protocol_constant<SOCK_STREAM> {
        };

        /// @brief UDP protocol type
        struct udp_t final : protocol_constant<SOCK_DGRAM> {
        };
    }

    /// @brief The concept for protocol types
    /// @tparam T The protocol type
    template <typename T>
    concept protocol = std::derived_from<T, detail_::protocol_constant<T::value>>;

    /// @brief TCP protocol
    constexpr inline detail_::tcp_t tcp;

    /// @brief UDP protocol
    constexpr inline detail_::udp_t udp;
}

#endif
