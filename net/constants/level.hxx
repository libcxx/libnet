#ifndef LIBNET_CONSTANTS_LEVEL
#define LIBNET_CONSTANTS_LEVEL

#ifdef _WIN32
    #include <winsock2.h>
#else
    #include <sys/socket.h>
#endif
#include <concepts>
#include <type_traits>

namespace net {
    namespace detail_ {
        /// @brief The native option level type
        using native_level = int;

        /// @brief A base class for option level types
        /// @tparam L The level value
        template <native_level L>
        struct level_constant : std::integral_constant<native_level, L> {
        };

        /// @brief The socket option level type
        struct socket_level_t final : level_constant<SOL_SOCKET> {
        };
    }

    /// @brief The concept for option levels
    /// @tparam T The level option type
    template <typename T>
    concept level = std::derived_from<T, detail_::level_constant<T::value>>;

    /// @brief The socket option level
    constexpr inline detail_::socket_level_t socket_level;
}

#endif
