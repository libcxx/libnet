#ifndef LIBNET_CONSTANTS_FAMILY
#define LIBNET_CONSTANTS_FAMILY

#ifdef _WIN32
    #include <winsock2.h>
#else
    #include <sys/socket.h>
#endif
#include <concepts>
#include <type_traits>
#include "net/address/ipv4_address.hxx"
#include "net/address/ipv6_address.hxx"

namespace net {
    namespace detail_ {
        /// @brief The native family type
        using native_family = int;

        /// @brief A base class for family types
        /// @tparam F The family value
        template <native_family F>
        struct family_constant : std::integral_constant<native_family, F> {
        };

        /// @brief IPv4 address family type
        struct ipv4_t final : family_constant<AF_INET> {
            using address_type = ipv4_address;
        };

        /// @brief IPv6 address family type
        struct ipv6_t final : family_constant<AF_INET6> {
            using address_type = ipv6_address;
        };
    }

    /// @brief The concept for family types
    /// @tparam T The family type
    template <typename T>
    concept family = std::derived_from<T, detail_::family_constant<T::value>> && requires {
        typename T::address_type;
    };

    /// @brief IPv4 address family
    constexpr inline detail_::ipv4_t ipv4;

    /// @brief IPv6 address family
    constexpr inline detail_::ipv6_t ipv6;
}

#endif
