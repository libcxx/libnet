#include <iostream>
#include <mutex>
#include <string>
#include <thread>
#include "net/net.hxx"

int main() {
    constexpr static auto localhost(net::ipv4_address::host(4200));

    std::mutex mutex;

    std::thread server([&]() {
        int debug = -1;
        auto server = net::make_socket(net::ipv4, net::tcp)
            | net::set_option(net::reuse_addr, 1)
            | net::bind(localhost)
            | net::listen(10);

        auto child = server
            | net::accept
            | net::get_option(net::debug, debug);

        auto debug2 = child | net::get_option(net::debug);

        if (child) {
            std::string buffer(15, '\0');
            child->receive(buffer);
            {
                std::unique_lock lock(mutex);
                std::cout << "(" << buffer << ")" << std::endl;
                std::cout << "debug: " << debug << std::endl;
                if (debug2) {
                    std::cout << "debug2: " << *debug2 << std::endl;
                }
            }

            child->send("Goodbye, world!");
        }
        else
        {
            {
                std::unique_lock lock(mutex);
                std::cerr << "server error: " << child.error().message() << "\n";
            }
        }
    });

    std::thread client([&]() {
        auto client = net::make_socket(net::ipv4, net::tcp)
            | net::connect(localhost);

        if (client) {
            client->send("Hello, world!");

            std::string buffer(15, '\0');
            client->receive(buffer);
            {
                std::unique_lock lock(mutex);
                std::cout << "(" << buffer << ")" << std::endl;
            }
        } else {
            {
                std::unique_lock lock(mutex);
                std::cerr << "client error: " << client.error().message() << "\n";
            }
        }
    });

    server.join();
    client.join();
}
